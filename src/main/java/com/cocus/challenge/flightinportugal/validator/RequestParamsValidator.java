package com.cocus.challenge.flightinportugal.validator;

import com.cocus.challenge.flightinportugal.constant.ErrorConstant;
import com.cocus.challenge.flightinportugal.exception.UnexpectedDateFormatException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedAirlineException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedCurrencyException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedOriginOrDestinationException;
import com.cocus.challenge.flightinportugal.model.Airline;
import com.cocus.challenge.flightinportugal.model.Airport;
import com.cocus.challenge.flightinportugal.model.Currency;
import com.cocus.challenge.flightinportugal.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.regex.Pattern;

@Slf4j
@Component
public class RequestParamsValidator {

    public void validateRequest(String origin, String destination, String dateFrom, String dateTo, String airlines, String currency)
        throws UnsupportedOriginOrDestinationException, UnexpectedDateFormatException, UnsupportedAirlineException, UnsupportedCurrencyException {

        validateOriginAndDestination(origin, destination);
        validateDateStringFormat(dateFrom, dateTo);
        validateAirlines(airlines);
        validateCurrency(currency);
    }

    private void validateDateStringFormat(String dateFrom, String dateTo) throws UnexpectedDateFormatException {
        if (StringUtil.isNullOrBlank(dateFrom) || StringUtil.isNullOrBlank(dateTo)) {
            log.error("--- Parameters date_from and date_to should not be null or empty");
            throw new IllegalArgumentException(ErrorConstant.INVALID_DATE);
        }

        Pattern pattern = Pattern.compile("^\\d{2}/\\d{2}/\\d{4}$");
        if (!pattern.matcher(dateFrom).matches() || !pattern.matcher(dateTo).matches()) {
            log.error("--- Invalid date format. Should be dd/mm/YYYY");
            throw new UnexpectedDateFormatException(ErrorConstant.DATE_FORMAT);
        }

    }

    private void validateOriginAndDestination(String origin, String destination) throws UnsupportedOriginOrDestinationException {
        if (StringUtil.isNullOrBlank(origin) || StringUtil.isNullOrBlank(destination)) {
            log.error("--- Parameters origin and destination should not be null or empty");
            throw new IllegalArgumentException(ErrorConstant.INVALID_ORIGIN_DESTINATION);
        }

        try {
            Airport.valueOf(origin);
            Airport.valueOf(destination);
        } catch (IllegalArgumentException e) {
            log.error("--- Unsupported origin or destination");
            throw new UnsupportedOriginOrDestinationException(ErrorConstant.UNSUPPORTED_ORIGIN_DESTINATION);
        }

        if (origin.equals(destination)) {
            log.error("--- Origin and destination must be different");
            throw new IllegalArgumentException(ErrorConstant.ORIGIN_EQUALS_DESTINATION);
        }

    }

    private void validateAirlines(String airlines) throws UnsupportedAirlineException {
        if (StringUtil.isNullOrBlank(airlines)) {
            log.error("--- Parameter airline should not be null or empty");
            throw new IllegalArgumentException(ErrorConstant.INVALID_AIRLINES);
        }

        String[] airlinesArray = airlines.split(",");

        try {
            Arrays.stream(airlinesArray).distinct().forEach(airline -> Airline.validateIataCode(airline.trim()));
        } catch (IllegalArgumentException e) {
            log.error("--- Unsupported airline");
            throw new UnsupportedAirlineException(ErrorConstant.UNSUPPORTED_AIRLINES);
        }

    }

    private void validateCurrency(String currency) throws UnsupportedCurrencyException {
        if (StringUtil.isNullOrBlank(currency)) {
            log.error("--- Parameter currency should not be null or empty");
            throw new IllegalArgumentException(ErrorConstant.INVALID_CURRENCY);
        }

        try {
            Currency.valueOf(currency);
        } catch (IllegalArgumentException e) {
            log.error("--- Unsupported currency");
            throw new UnsupportedCurrencyException(ErrorConstant.UNSUPPORTED_CURRENCY);
        }

    }
}
