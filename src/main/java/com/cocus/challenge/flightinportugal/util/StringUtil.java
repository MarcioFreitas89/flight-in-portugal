package com.cocus.challenge.flightinportugal.util;

public class StringUtil {

    public static boolean isNullOrBlank(String string) {
        return string == null || string.isBlank();
    }

    private StringUtil() {
    }
}
