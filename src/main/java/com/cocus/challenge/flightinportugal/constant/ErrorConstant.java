package com.cocus.challenge.flightinportugal.constant;

public class ErrorConstant {

    public static final String UNSUPPORTED_ORIGIN_DESTINATION = "The application does not support the chosen origin or destination";
    public static final String ORIGIN_EQUALS_DESTINATION = "The chosen origin and destination must be different";
    public static final String INVALID_ORIGIN_DESTINATION = "Parameters origin and destination should not be null, empty or blank";
    public static final String UNSUPPORTED_AIRLINES = "The application does not support the chosen airline(s)";
    public static final String INVALID_AIRLINES = "Parameter airline should not be null, empty or blank";
    public static final String UNSUPPORTED_CURRENCY = "The application does not support the chosen currency";
    public static final String INVALID_CURRENCY = "Parameter currency should not be null, empty or blank";
    public static final String DATE_FORMAT = "Invalid date format. The pattern should be dd/mm/YYYY";
    public static final String INVALID_DATE= "Parameter date should not be null, empty or blank";


    private ErrorConstant() {
    }
}
