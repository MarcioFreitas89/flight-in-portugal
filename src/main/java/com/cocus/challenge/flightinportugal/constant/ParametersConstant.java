package com.cocus.challenge.flightinportugal.constant;

public class ParametersConstant {

    public static final String PARTNER = "partner";
    public static final String PARTNER_VALUE = "picky";
    public static final String ORIGIN = "fly_from";
    public static final String DESTINATION = "fly_to";
    public static final String DATE_FROM = "date_from";
    public static final String DATE_TO = "date_to";
    public static final String AIRLINES = "select_airlines";
    public static final String CURRENCY = "curr";

    private ParametersConstant() {
    }

}


