package com.cocus.challenge.flightinportugal.controller;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import com.cocus.challenge.flightinportugal.service.RequestService;
import com.cocus.challenge.flightinportugal.service.RequestServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/api/request-store")
@Slf4j
public class RequestControllerImpl implements RequestController {

    private final RequestService requestService;

    public RequestControllerImpl(RequestServiceImpl requestService) {
        this.requestService = requestService;
    }

    @Override
    public ResponseEntity<Page<RequestEntity>> getAllRequests(Pageable pageable) {
        log.info("--- Received list all RequestEntity request.");
        Page<RequestEntity> requests = requestService.getAllRequests(pageable);

        return new ResponseEntity<>(requests, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<String> deleteAllRequests() {
        log.info("--- Received delete all RequestEntity request.");
        requestService.deleteAllRequests();
        return new ResponseEntity<>("All requests have been delete from database", HttpStatus.OK);
    }
}