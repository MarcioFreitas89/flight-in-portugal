package com.cocus.challenge.flightinportugal.controller;

import com.cocus.challenge.flightinportugal.exception.UnexpectedDateFormatException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedAirlineException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedCurrencyException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedOriginOrDestinationException;
import com.cocus.challenge.flightinportugal.model.dto.AverageFlightPrice;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

public interface FlightController {

    @GetMapping(value = "/flights-avg-price", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<AverageFlightPrice> getFlightsAveragePrice(
        @RequestParam(value = "fly_from") String origin,
        @RequestParam(value = "fly_to") String destination,
        @RequestParam(value = "date_from") String dateFrom,
        @RequestParam(value = "date_to") String dateTo,
        @RequestParam(value = "select_airlines") String airlines,
        @RequestParam(value = "curr") String currency
    ) throws UnexpectedDateFormatException, UnsupportedAirlineException, UnsupportedOriginOrDestinationException, UnsupportedCurrencyException;

}
