package com.cocus.challenge.flightinportugal.controller;

import com.cocus.challenge.flightinportugal.exception.UnexpectedDateFormatException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedAirlineException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedCurrencyException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedOriginOrDestinationException;
import com.cocus.challenge.flightinportugal.model.RequestEntity;
import com.cocus.challenge.flightinportugal.model.dto.AverageFlightPrice;
import com.cocus.challenge.flightinportugal.service.FlightService;
import com.cocus.challenge.flightinportugal.service.FlightServiceImpl;
import com.cocus.challenge.flightinportugal.service.RequestService;
import com.cocus.challenge.flightinportugal.service.RequestServiceImpl;
import com.cocus.challenge.flightinportugal.validator.RequestParamsValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/api/flight-in-portugal")
@Slf4j
public class FlightControllerImpl implements FlightController {

    private final FlightService flightService;
    private final RequestService requestService;
    private final RequestParamsValidator validator;

    public FlightControllerImpl(FlightServiceImpl service, RequestServiceImpl requestService,
        RequestParamsValidator validator) {
        this.flightService = service;
        this.requestService = requestService;
        this.validator = validator;
    }

    @Override
    public ResponseEntity<AverageFlightPrice> getFlightsAveragePrice(String origin, String destination, String dateFrom, String dateTo,
        String airlines, String currency)
        throws UnexpectedDateFormatException, UnsupportedAirlineException, UnsupportedOriginOrDestinationException, UnsupportedCurrencyException {

        log.info("--- Received new average price request");

        RequestEntity requestEntity = requestService.saveNewRequest(origin, destination, dateFrom, dateTo, currency);
        if (requestEntity == null) {
            log.error("--- Could not save the received request on database");
        }

        validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);

        AverageFlightPrice result = flightService.getAveragePrice(origin, destination, dateFrom, dateTo, airlines, currency);

        if (result == null) {
            log.info("--- No flight's data was found to the given parameters");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
