package com.cocus.challenge.flightinportugal.controller;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;

public interface RequestController {

    @GetMapping(value = "/requests", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<Page<RequestEntity>> getAllRequests(@PageableDefault(value = 20) Pageable pageable);

    @DeleteMapping(value = "/requests", consumes = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity<String> deleteAllRequests();

}
