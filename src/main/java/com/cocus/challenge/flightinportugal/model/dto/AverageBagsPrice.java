package com.cocus.challenge.flightinportugal.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AverageBagsPrice implements Serializable {

    private static final long serialVersionUID = 2250951576641826424L;

    private double firstBag;
    private double secondBag;
}
