package com.cocus.challenge.flightinportugal.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Setter
@Getter
@Builder
@Document(collection = "requests")
public class RequestEntity {

    @Id
    private String id;
    private String origin;
    private String destination;
    private String dateFrom;
    private String dateTo;
    private String currency;

}
