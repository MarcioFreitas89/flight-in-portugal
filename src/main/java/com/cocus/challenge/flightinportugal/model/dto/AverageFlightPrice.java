package com.cocus.challenge.flightinportugal.model.dto;

import com.cocus.challenge.flightinportugal.model.Airport;
import com.cocus.challenge.flightinportugal.model.Currency;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AverageFlightPrice implements Serializable {

    private static final long serialVersionUID = -5419557323014955717L;

    private Airport originAirport;
    private Airport destinationAirport;
    private Double averagePrice;
    private AverageBagsPrice averageBagsPrice;
    private Currency currency;

}
