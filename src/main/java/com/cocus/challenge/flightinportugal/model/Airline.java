package com.cocus.challenge.flightinportugal.model;

import lombok.Getter;

@Getter
public enum Airline {
    TAP("TP"),
    RYANAIR("FR");

    private final String iataCode;

    Airline(String iataCode) {
        this.iataCode = iataCode;
    }

    public static void validateIataCode(String iataCode) {
        for (Airline airline : Airline.values()) {
            if (airline.iataCode.equals(iataCode)) {
                return;
            }
        }
        throw new IllegalArgumentException();
    }

}
