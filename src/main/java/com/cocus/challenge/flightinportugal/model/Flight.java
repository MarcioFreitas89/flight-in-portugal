package com.cocus.challenge.flightinportugal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Flight {

    @JsonProperty("price")
    private Double price;
    @JsonProperty("bags_price")
    private Map<String, Double> bagsPrice;
    @JsonProperty("conversion")
    private Map<String, Double> currency;
    @JsonProperty("flyFrom")
    private String origin;
    @JsonProperty("flyTo")
    private String destination;

}
