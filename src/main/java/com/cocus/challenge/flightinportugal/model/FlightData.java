package com.cocus.challenge.flightinportugal.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.util.List;

@Getter
public class FlightData {

    @JsonProperty("data")
    private List<Flight> flights;
}
