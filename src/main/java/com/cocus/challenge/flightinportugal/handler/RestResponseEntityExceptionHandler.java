package com.cocus.challenge.flightinportugal.handler;

import com.cocus.challenge.flightinportugal.exception.UnexpectedDateFormatException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedAirlineException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedCurrencyException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedOriginOrDestinationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    public RestResponseEntityExceptionHandler() {
        super();
    }

    @ExceptionHandler(value = {IllegalArgumentException.class})
    protected ResponseEntity<Object> handleBadRequest(IllegalArgumentException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {UnsupportedOriginOrDestinationException.class})
    protected ResponseEntity<Object> handleBadRequest(UnsupportedOriginOrDestinationException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {UnexpectedDateFormatException.class})
    protected ResponseEntity<Object> handleBadRequest(UnexpectedDateFormatException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {UnsupportedCurrencyException.class})
    protected ResponseEntity<Object> handleBadRequest(UnsupportedCurrencyException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {UnsupportedAirlineException.class})
    protected ResponseEntity<Object> handleBadRequest(UnsupportedAirlineException ex, WebRequest request) {
        String bodyOfResponse = ex.getMessage();
        return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

}
