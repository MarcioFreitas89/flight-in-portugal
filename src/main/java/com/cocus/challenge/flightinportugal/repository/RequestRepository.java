package com.cocus.challenge.flightinportugal.repository;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RequestRepository extends MongoRepository<RequestEntity, String> {

}
