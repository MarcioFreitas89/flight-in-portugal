package com.cocus.challenge.flightinportugal.service;

import com.cocus.challenge.flightinportugal.client.FlightClient;
import com.cocus.challenge.flightinportugal.client.FlightClientImpl;
import com.cocus.challenge.flightinportugal.constant.ParametersConstant;
import com.cocus.challenge.flightinportugal.model.Airport;
import com.cocus.challenge.flightinportugal.model.Currency;
import com.cocus.challenge.flightinportugal.model.Flight;
import com.cocus.challenge.flightinportugal.model.FlightData;
import com.cocus.challenge.flightinportugal.model.dto.AverageBagsPrice;
import com.cocus.challenge.flightinportugal.model.dto.AverageFlightPrice;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.List;
import java.util.OptionalDouble;

@Service
@Slf4j
public class FlightServiceImpl implements FlightService {

    private final FlightClient client;

    public FlightServiceImpl(FlightClientImpl client) {
        this.client = client;
    }

    @Override
    @Cacheable(value = "avg-prices")
    public AverageFlightPrice getAveragePrice(String origin, String destination, String dateFrom, String dateTo, String airlines, String currency) {

        log.info("--- Reached the service. This request is not cached yet");

        FlightData flightData = client.sendRequest(createQueryParameters(origin, destination, dateFrom, dateTo, airlines, currency));

        if (flightData.getFlights() == null || flightData.getFlights().isEmpty()){
            return null;
        }

        return AverageFlightPrice.builder()
                                 .originAirport(Airport.valueOf(origin))
                                 .destinationAirport(Airport.valueOf(destination))
                                 .currency(Currency.valueOf(currency))
                                 .averagePrice(calculateFlightAveragePrice(flightData.getFlights()))
                                 .averageBagsPrice(calculateBagsAveragePrice(flightData.getFlights()))
                                 .build();
    }

    private MultiValueMap<String, String> createQueryParameters(String origin, String destination, String dateFrom, String dateTo, String airlines,
        String currency) {

        MultiValueMap<String, String> queryParameters = new LinkedMultiValueMap<>();
        queryParameters.add(ParametersConstant.ORIGIN, origin);
        queryParameters.add(ParametersConstant.DESTINATION, destination);
        queryParameters.add(ParametersConstant.DATE_FROM, dateFrom);
        queryParameters.add(ParametersConstant.DATE_TO, dateTo);
        queryParameters.add(ParametersConstant.AIRLINES, airlines);
        queryParameters.add(ParametersConstant.CURRENCY, currency);
        queryParameters.add(ParametersConstant.PARTNER, ParametersConstant.PARTNER_VALUE);

        return queryParameters;
    }

    private Double calculateFlightAveragePrice(List<Flight> flights) {
        OptionalDouble optionalDouble = flights.stream().mapToDouble(Flight::getPrice).average();

        if (optionalDouble.isPresent()) {
            return optionalDouble.getAsDouble();
        }

        log.info("--- Could not calculate the flight average");
        return null;
    }

    private AverageBagsPrice calculateBagsAveragePrice(List<Flight> flights) {
        AverageBagsPrice.AverageBagsPriceBuilder avgBagsPriceBuilder = AverageBagsPrice.builder();

        flights.stream()
               .filter(flight -> !flight.getBagsPrice().isEmpty())
               .mapToDouble(flight -> flight.getBagsPrice().get("1"))
               .average().ifPresent(avgBagsPriceBuilder::firstBag);

        flights.stream()
               .filter(flight -> flight.getBagsPrice().size() == 2)
               .mapToDouble(flight -> flight.getBagsPrice().get("2"))
               .average().ifPresent(avgBagsPriceBuilder::secondBag);

        return avgBagsPriceBuilder.build();
    }
}
