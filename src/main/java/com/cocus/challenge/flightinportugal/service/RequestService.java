package com.cocus.challenge.flightinportugal.service;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RequestService {

    RequestEntity saveNewRequest(String origin, String destination, String dateFrom, String dateTo, String currency);

    void deleteAllRequests();

    Page<RequestEntity> getAllRequests(Pageable pageable);

}
