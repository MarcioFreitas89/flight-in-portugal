package com.cocus.challenge.flightinportugal.service;

import com.cocus.challenge.flightinportugal.model.dto.AverageFlightPrice;

public interface FlightService {

    AverageFlightPrice getAveragePrice(String origin, String destination, String dateFrom, String dateTo, String airlines, String currency);

}
