package com.cocus.challenge.flightinportugal.service;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import com.cocus.challenge.flightinportugal.repository.RequestRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository repository;

    public RequestServiceImpl(RequestRepository repository) {
        this.repository = repository;
    }

    public RequestEntity saveNewRequest(String origin, String destination, String dateFrom, String dateTo, String currency) {
        RequestEntity requestEntity = RequestEntity.builder()
                                                   .origin(origin)
                                                   .destination(destination)
                                                   .dateFrom(dateFrom)
                                                   .dateTo(dateTo)
                                                   .currency(currency)
                                                   .build();
        return repository.insert(requestEntity);
    }

    public void deleteAllRequests() {
        repository.deleteAll();
    }

    public Page<RequestEntity> getAllRequests(Pageable pageable) {
        return repository.findAll(pageable);
    }
}
