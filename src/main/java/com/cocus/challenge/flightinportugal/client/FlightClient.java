package com.cocus.challenge.flightinportugal.client;

import com.cocus.challenge.flightinportugal.model.FlightData;
import org.springframework.util.MultiValueMap;

public interface FlightClient {

    FlightData sendRequest(MultiValueMap<String, String> queryParameters);
}
