package com.cocus.challenge.flightinportugal.client;

import com.cocus.challenge.flightinportugal.model.FlightData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;

@Component
@Slf4j
public class FlightClientImpl implements FlightClient {

    private static final String BASE_URL = "https://api.skypicker.com/flights";

    private final WebClient client;

    public FlightClientImpl() {
        this.client = WebClient.builder()
                               .baseUrl(BASE_URL)
                               .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                               .defaultHeader(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE)
                               .build();
    }

    public FlightData sendRequest(MultiValueMap<String, String> queryParameters) {
        log.info("--- Sending http request to {}", BASE_URL);

        return client.get()
                     .uri(uriBuilder -> uriBuilder.queryParams(queryParameters)
                                                  .build())
                     .retrieve()
                     .bodyToMono(FlightData.class)
                     .block();
    }
}
