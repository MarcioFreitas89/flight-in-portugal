package com.cocus.challenge.flightinportugal.exception;

public class UnexpectedDateFormatException extends Exception {

    public UnexpectedDateFormatException(String message){
        super(message);
    }
}
