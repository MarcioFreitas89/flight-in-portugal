package com.cocus.challenge.flightinportugal.exception;

public class UnsupportedCurrencyException extends Exception {

    public UnsupportedCurrencyException(String message){
        super(message);
    }
}
