package com.cocus.challenge.flightinportugal.exception;

public class UnsupportedOriginOrDestinationException extends Exception {

    public UnsupportedOriginOrDestinationException(String message){
        super(message);
    }
}
