package com.cocus.challenge.flightinportugal.exception;

public class UnsupportedAirlineException extends Exception {

    public UnsupportedAirlineException(String message){
        super(message);
    }
}
