package com.cocus.challenge.flightinportugal.validator;

import com.cocus.challenge.flightinportugal.exception.UnexpectedDateFormatException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedAirlineException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedCurrencyException;
import com.cocus.challenge.flightinportugal.exception.UnsupportedOriginOrDestinationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RequestParamsValidatorTest {

    private RequestParamsValidator validator;
    private String origin;
    private String destination;
    private String dateFrom;
    private String dateTo;
    private String currency;
    private String airlines;

    @BeforeEach
    void setup() {
        validator = new RequestParamsValidator();
        origin = "LIS";
        destination = "OPO";
        dateFrom = "13/03/2021";
        dateTo = "14/03/2021";
        currency = "EUR";
        airlines = "TP, FR";
    }

    @Test
    void validateRequest_withValidParameters_returnTrue() {
        //arrange

        //act and assert
        assertDoesNotThrow(() -> validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency));

    }

    @Test
    void validateRequest_withInvalidOrigin_returnFalse() {
        //arrange
        origin = "OOO";

        //act and assert
        assertThrows(UnsupportedOriginOrDestinationException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withInvalidDestination_returnFalse() {
        //arrange
        destination = "AAA";

        //act and assert
        assertThrows(UnsupportedOriginOrDestinationException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withOriginEqualsDestination_returnFalse() {
        //arrange
        origin = "OPO";

        //act and assert
        assertThrows(IllegalArgumentException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withNullOrigin_returnFalse() {
        //arrange

        //act and assert
        assertThrows(IllegalArgumentException.class, () -> {
            validator.validateRequest(null, destination, dateFrom, dateTo, airlines, currency);
        });
    }

    @Test
    void validateRequest_withInvalidDateFrom_returnFalse() {
        //arrange
        dateFrom = "1/3/21";

        //act and assert
        assertThrows(UnexpectedDateFormatException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withNullDateFrom_returnFalse() {
        //arrange

        //act and assert
        assertThrows(IllegalArgumentException.class, () -> {
            validator.validateRequest(origin, destination, null, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withInvalidDateTo_returnFalse() {
        //arrange
        dateTo = "date";

        //act and assert
        assertThrows(UnexpectedDateFormatException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withBlankDateTo_returnFalse() {
        //arrange
        dateTo = " ";

        //act and assert
        assertThrows(IllegalArgumentException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withInvalidCurrency_returnFalse() {
        //arrange
        currency = "BRL";

        //act and assert
        assertThrows(UnsupportedCurrencyException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });
    }

    @Test
    void validateRequest_withNullCurrency_returnFalse() {
        //arrange

        //act and assert
        assertThrows(IllegalArgumentException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, null);
        });

    }

    @Test
    void validateRequest_withInvalidAirlines_returnFalse() {
        //arrange
        airlines = "MN, PO";

        //act and assert
        assertThrows(UnsupportedAirlineException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });

    }

    @Test
    void validateRequest_withBlankAirlines_returnFalse() {
        //arrange
        airlines = "   ";

        //act and assert
        assertThrows(IllegalArgumentException.class, () -> {
            validator.validateRequest(origin, destination, dateFrom, dateTo, airlines, currency);
        });
    }
}
