package com.cocus.challenge.flightinportugal.service;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import com.cocus.challenge.flightinportugal.repository.RequestRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RequestServiceImplTest {

    @Mock
    private RequestRepository repository;
    @InjectMocks
    private RequestServiceImpl service;

    @Test
    void saveNewRequest_withValidRequestEntity_invokeInsertOnce() {
        // arrange
        String origin = "OPO";
        String destination = "LIS";
        String currency = "EUR";
        String dateFrom = "10/03/2021";
        String dateTo = "11/03/2021";

        RequestEntity requestEntity = RequestEntity.builder().build();
        when(repository.insert(any(RequestEntity.class))).thenReturn(requestEntity);

        //act
        RequestEntity result = service.saveNewRequest(origin, destination, dateFrom, dateTo, currency);

        //assert
        Assertions.assertEquals(requestEntity, result);
    }

    @Test
    void getAllRequests_withNoParameters_returnListWithAllRequests() {
        // arrange
        Page<RequestEntity> requestEntities = new PageImpl<>( new ArrayList<>());
        Pageable pageable = PageRequest.of(0, 50);
        when(repository.findAll(pageable)).thenReturn(requestEntities);

        //act
        Page<RequestEntity> result = service.getAllRequests(pageable);

        //assert
        Assertions.assertEquals(requestEntities, result);
    }

    @Test
    void deleteAllRequests_withNoParameters_invokeDeleteAllOnce() {
        // arrange
        doNothing().when(repository).deleteAll();

        //act
        service.deleteAllRequests();

        //assert
        verify(repository, times(1)).deleteAll();
    }

}
