package com.cocus.challenge.flightinportugal.service;

import com.cocus.challenge.flightinportugal.client.FlightClientImpl;
import com.cocus.challenge.flightinportugal.model.FlightData;
import com.cocus.challenge.flightinportugal.model.dto.AverageFlightPrice;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.util.LinkedMultiValueMap;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightServiceImplTest {

    @Mock
    private FlightClientImpl flightClient;
    @InjectMocks
    private FlightServiceImpl flightService;

    private FlightData flightData;

    @BeforeEach
    void setup() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("test-data/flightData.json");
        flightData = objectMapper.readValue(Objects.requireNonNull(inputStream), FlightData.class);
    }

    @Test
    void getAveragePrice_withValidQueryParameters_returnCorrectCalculateAveragePrice() {
        //arrange
        String origin = "OPO";
        String destination = "LIS";
        String currency = "EUR";
        String dateFrom = "10/03/2021";
        String dateTo = "11/03/2021";
        String airlines = "TP,FR";

        when(flightClient.sendRequest(any(LinkedMultiValueMap.class))).thenReturn(flightData);

        //act
        AverageFlightPrice result = flightService.getAveragePrice(origin, destination, dateFrom, dateTo, airlines, currency);

        //assert
        assertEquals(origin, result.getOriginAirport().toString());
        assertEquals(destination, result.getDestinationAirport().toString());
        assertEquals(144.5d, result.getAveragePrice());
        assertEquals(currency, result.getCurrency().toString());
        assertEquals(48.2d, result.getAverageBagsPrice().getFirstBag());
        assertEquals(123.9d, result.getAverageBagsPrice().getSecondBag());
    }
}
