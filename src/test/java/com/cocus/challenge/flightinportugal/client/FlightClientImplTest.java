package com.cocus.challenge.flightinportugal.client;

import com.cocus.challenge.flightinportugal.model.FlightData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.util.ReflectionTestUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightClientImplTest {

    private FlightClientImpl flightClient;
    @Mock
    private WebClient webClientMock;
    @Mock
    private FlightData flightDataMock;
    @Mock
    private WebClient.RequestHeadersSpec requestHeadersMock;
    @Mock
    private WebClient.RequestHeadersUriSpec requestHeadersUriMock;
    @Mock
    private WebClient.ResponseSpec responseMock;

    @BeforeEach
    void setup() {
        flightClient = new FlightClientImpl();
        ReflectionTestUtils.setField(flightClient, "client", webClientMock);

    }

    @Test
    void sendRequest_withValidParameters_returnFlightData() {
        //arrange
        MultiValueMap<String, String> parameters = new LinkedMultiValueMap<>();
        when(webClientMock.get()).thenReturn(requestHeadersUriMock);
        when(requestHeadersUriMock.uri(any(Function.class))).thenReturn(requestHeadersMock);
        when(requestHeadersMock.retrieve()).thenReturn(responseMock);
        when(responseMock.bodyToMono(FlightData.class)).thenReturn(Mono.just(flightDataMock));

        //act
        FlightData result = flightClient.sendRequest(parameters);

        //assert
        assertEquals(flightDataMock, result);
    }

}
