package com.cocus.challenge.flightinportugal.controller;

import com.cocus.challenge.flightinportugal.constant.ParametersConstant;
import com.cocus.challenge.flightinportugal.exception.UnsupportedOriginOrDestinationException;
import com.cocus.challenge.flightinportugal.handler.RestResponseEntityExceptionHandler;
import com.cocus.challenge.flightinportugal.model.Airport;
import com.cocus.challenge.flightinportugal.model.Currency;
import com.cocus.challenge.flightinportugal.model.RequestEntity;
import com.cocus.challenge.flightinportugal.model.dto.AverageBagsPrice;
import com.cocus.challenge.flightinportugal.model.dto.AverageFlightPrice;
import com.cocus.challenge.flightinportugal.service.FlightServiceImpl;
import com.cocus.challenge.flightinportugal.service.RequestServiceImpl;
import com.cocus.challenge.flightinportugal.validator.RequestParamsValidator;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FlightControllerImplTest {

    private static final String REQUEST_URI = "http://localhost:8089/v1/api/flight-in-portugal/flights-avg-price";

    @Mock
    private FlightServiceImpl flightServiceMock;
    @Mock
    private RequestParamsValidator validatorMock;
    @Mock
    private RequestServiceImpl requestServiceMock;
    @InjectMocks
    private FlightControllerImpl flightController;

    private MockMvc mockMvc;
    private MultiValueMap<String, String> queryParameters;
    @Mock
    private RequestEntity requestEntityMock;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(flightController).setControllerAdvice(new RestResponseEntityExceptionHandler()).build();

        queryParameters = new LinkedMultiValueMap<>();
        queryParameters.add(ParametersConstant.ORIGIN, "OPO");
        queryParameters.add(ParametersConstant.DESTINATION, "LIS");
        queryParameters.add(ParametersConstant.DATE_FROM, "15/03/2021");
        queryParameters.add(ParametersConstant.DATE_TO, "16/03/2021");
        queryParameters.add(ParametersConstant.AIRLINES, "TP,FR");
        queryParameters.add(ParametersConstant.CURRENCY, "EUR");
        queryParameters.add(ParametersConstant.PARTNER, ParametersConstant.PARTNER_VALUE);
    }

    @Test
    void getFlightsAveragePrice_withValidQueryParams_returnStatusOkAndAverageFlightsPrice() throws Exception {
        // arrange
        AverageFlightPrice averageFlightPrice = AverageFlightPrice.builder()
                                                                  .originAirport(Airport.OPO)
                                                                  .destinationAirport(Airport.LIS)
                                                                  .currency(Currency.EUR)
                                                                  .averagePrice(100.0)
                                                                  .averageBagsPrice(AverageBagsPrice.builder()
                                                                                                    .firstBag(50.0)
                                                                                                    .secondBag(50.0)
                                                                                                    .build())
                                                                  .build();
        ObjectMapper objectMapper = new ObjectMapper();

        when(flightServiceMock.getAveragePrice(anyString(), anyString(), anyString(), anyString(), anyString(), anyString()))
            .thenReturn(averageFlightPrice);
        doNothing().when(validatorMock).validateRequest(anyString(), anyString(), anyString(), anyString(), anyString(), anyString());
        when(requestServiceMock.saveNewRequest(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(requestEntityMock);

        //act and assert
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get(REQUEST_URI)
                                                                    .queryParams(queryParameters)
                                                                    .contentType(MediaType.APPLICATION_JSON))
                                     .andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

        AverageFlightPrice result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), AverageFlightPrice.class);

        assertEquals(averageFlightPrice.getAveragePrice(), result.getAveragePrice());
        assertEquals(averageFlightPrice.getAverageBagsPrice().getFirstBag(), result.getAverageBagsPrice().getFirstBag());
        assertEquals(averageFlightPrice.getAverageBagsPrice().getSecondBag(), result.getAverageBagsPrice().getSecondBag());
        assertEquals(averageFlightPrice.getCurrency(), result.getCurrency());
        assertEquals(averageFlightPrice.getOriginAirport(), result.getOriginAirport());
        assertEquals(averageFlightPrice.getDestinationAirport(), result.getDestinationAirport());
    }

    @Test
    void getFlightsAveragePrice_withInValidQueryParams_returnStatusBadRequest() throws Exception {
        // arrange
        when(requestServiceMock.saveNewRequest(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(requestEntityMock);
        doThrow(UnsupportedOriginOrDestinationException.class)
            .when(validatorMock).validateRequest(anyString(), anyString(), anyString(), anyString(), anyString(), anyString());

        //act and assert
        mockMvc.perform(MockMvcRequestBuilders.get(REQUEST_URI).queryParams(queryParameters).contentType(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.status().isBadRequest())
               .andExpect(mvcResult -> assertTrue(mvcResult.getResolvedException() instanceof UnsupportedOriginOrDestinationException));
    }

    @Test
    void getFlightsAveragePrice_withValidAndNullServiceResponse_returnStatusInternalServerError() throws Exception {
        // arrange
        when(flightServiceMock.getAveragePrice(anyString(), anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(null);
        doNothing().when(validatorMock).validateRequest(anyString(), anyString(), anyString(), anyString(), anyString(), anyString());
        when(requestServiceMock.saveNewRequest(anyString(), anyString(), anyString(), anyString(), anyString())).thenReturn(requestEntityMock);

        //act and assert
        mockMvc.perform(MockMvcRequestBuilders.get(REQUEST_URI).queryParams(queryParameters).contentType(MediaType.APPLICATION_JSON))
               .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
