package com.cocus.challenge.flightinportugal.controller;

import com.cocus.challenge.flightinportugal.model.RequestEntity;
import com.cocus.challenge.flightinportugal.service.RequestServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;

import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RequestControllerImplTest {

    private static final String REQUEST_URI = "http://localhost:8089/v1/api/request-store/requests";

    @Mock
    private RequestServiceImpl requestServiceMock;
    @InjectMocks
    private RequestControllerImpl requestController;
    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(requestController)
                                 .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                                 .build();
    }

    @Test
    void getAllRequests_withValidPageable_returnStatusCodeOk() throws Exception {
        //arrange
        Page<RequestEntity> requestEntities = new PageImpl<>(new ArrayList<>());
        when(requestServiceMock.getAllRequests(Mockito.any(Pageable.class))).thenReturn(requestEntities);

        //act and assert
        mockMvc.perform(MockMvcRequestBuilders.get(REQUEST_URI)
                                              .contentType(MediaType.APPLICATION_JSON)
                                              .queryParam("page", "0")
                                              .queryParam("size", "10"))
               .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    void deleteAllRequests_withNoParametersRequired_returnAllRequests() throws Exception {
        //act and assert
        mockMvc.perform(MockMvcRequestBuilders.get(REQUEST_URI).contentType(MediaType.APPLICATION_JSON)).andExpect(
            MockMvcResultMatchers.status().isOk());
    }

}
